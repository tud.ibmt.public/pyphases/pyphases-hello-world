from pyPhases import Phase


class SayIt(Phase):
    """This phase says the sentence."""

    def main(self):
        sentence = self.getData("sentence", str)
        self.logSuccess(sentence)
