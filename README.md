# pyPhases Hello World

This is a simple example of how to use pyPhases. 

- Install pyPhases with `pip install pyPhases phases`
- Run it with `phases run SayIt`

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tud.ibmt.public/pyphases/pyphases-hello-world)